import pandas as pd
import numpy as np
import scipy.stats as si
import sympy as sy


###QUESTION 1: Analytical way
def anal_B_Scholes(S, K, T, r, q, sigma):
    #Where
    # S: spot price
    # K: strike price
    # T: time to maturity
    # r: interest rate
    # q: dividend yield
    # sigma: volatility of underlying asset

    d1 = (np.log(S / K) + (r - q + 0.5 * sigma ** 2) * T) / (sigma * np.sqrt(T))
    d2 = (np.log(S / K) + (r - q - 0.5 * sigma ** 2) * T) / (sigma * np.sqrt(T))

    Call_Price = (S * si.norm.cdf(d1, 0.0, 1.0) - K * np.exp(-r * T) * si.norm.cdf(d2, 0.0, 1.0))

    return Call_Price

print(anal_B_Scholes(100,50,1,0.01,0.02,1))


###QUESTION 2: geometric Brownian motion
from math import sqrt, log, exp, erf
import random
from numpy import arange
import matplotlib.pyplot as plt
import os
from random import gauss

def geom_BS(S,v,r,T):
    S1 = S * exp((r - 0.5 * v ** 2) * T + v * sqrt(T) * gauss(0, 1.0))
    return S1


S0 = 100.0  # S0 = Stock price
strikes = [i for i in range(50, 150)]  # Exercise prices range
T = 1  # T = Time to expiration
r = 0.01  # r = risk-free interest rate
q = 0.02  # q = dividend yield
v = 0.2  # v = volatility
Nsteps = 100  # Number or steps in MC
payoffs = []
discount_factor = exp(-r * T)

def call_payoff(S_T,strikes):
    tmp = [(S0 - strike) for strike in strikes]
    tmp.append(0.0)
    Max1 = max(tmp)
    return Max1

for i in range(0, 100):
    S_T = geom_BS(S0,v,r,T)
    payoffs.append(
        call_payoff(S_T, strikes)
    )

price = discount_factor * (sum(payoffs) / float(Nsteps))
print('Price: %.4f' % price)


###QUESTION 3

